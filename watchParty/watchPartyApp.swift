//
//  watchPartyApp.swift
//  watchParty
//
//  Created by Josiah Pederson on 4/3/23.
//

import SwiftUI

@main
struct watchPartyApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
